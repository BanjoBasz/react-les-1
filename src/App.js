import React from "react";

import "./App.css";
import ImgCardList from "./ImgCardList";

//Arrow function
class App extends React.Component {
  cardClicked = id => {
    console.log("hallo je klikt op een kaart " + id);
  };
  render() {
    return <ImgCardList cardClicked={this.cardClicked} />;
  }
}

export default App;
