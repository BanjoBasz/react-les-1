import React from "react";
import ImgCard from "./ImgCard";

class ImgCardList extends React.Component {
  cardClicked = id => {
    this.props.cardClicked(id);
  };

  listClicked = () => {
    console.log("klik op lijst");
  };

  render() {
    return (
      <div className="container" onClick={this.listClicked}>
        <ImgCard
          id="1"
          title="Nieuwe game DBZ"
          text="De makers van DBZ hebben een nieuwe game uitgebracht op de SWITCH"
          buttonText="Game review"
          cardClicked={this.cardClicked}
          background="/img/dbz.png"
        />
        <ImgCard
          id="2"
          title="Nieuw Sushi restaurant"
          text="Robbert heeft een nieuw sushi restaurant geopend in Delft"
          buttonText="Winkel hier!"
          cardClicked={this.cardClicked}
          background="/img/sushi.jpg"
        />
        <ImgCard
          id="3"
          title="RTL heeft een nieuwe show met Chantal Jansen"
          text="RTL heeft een nieuwe show over iets vaags"
          buttonText="Spoilers zien?"
          cardClicked={this.cardClicked}
          background="/img/chantal.jpg"
        />
        <ImgCard id="4" cardClicked={this.cardClicked} />
      </div>
    );
  }
}

export default ImgCardList;
