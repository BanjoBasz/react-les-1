import React from "react";

import "./ImgCard.css";

class ImgCard extends React.Component {
  onCardClicked = () => {
    this.props.cardClicked(this.props.id);
  };

  render() {
    return (
      <div className="img-card mdl-card mdl-shadow--2dp">
        <div
          className="mdl-card__title mdl-card--expand"
          style={{ backgroundImage: `url(${this.props.background})` }}
        >
          <h2 className="mdl-card__title-text">
            {this.props.title || "Artikel"}
          </h2>
        </div>
        <div className="mdl-card__supporting-text">
          {this.props.text || "Geen tekst beschikbaar"}
        </div>
        <div className="mdl-card__actions mdl-card--border">
          <a
            onClick={this.onCardClicked}
            className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"
          >
            {this.props.buttonText || "Lees meer"}
          </a>
        </div>
      </div>
    );
  }
}

export default ImgCard;
